import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.annotations.TestInstance;


public class MAIN_MENU_USER_INTERFACE {

    //Verifying the splash screen
    @Test(priority = 1)
    public void C001_Splash_Screen_Verification() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();
        Thread.sleep(5000);
        TestBase.get_Screenshot("src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C001_Splash_Screen_Verification/" + "Splash screen" + ".jpg");
    }


    // Verifiying search destination screen in dashboard screen
    @Test(priority = 2)
    public void C002_Verify_the_Set_Destination_Page() throws Exception {
        TestBase.get_Screenshot("src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C002_Verify_the_Set_Destination_Page/" + "Dashboard_Screen" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Search_option"), "C001: Search text is not present in the Daashboard screen");
        TestBase.getObject("Search_option").click();
        TestBase.driver.navigate().back();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\check_for_search_destination_screen/" + "Search_Destination_screen" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Recent_button"), "C002:Recent text is not present in the Search_Destination_screen");
        Assert.assertTrue(TestBase.isElementPresent("POI"), "C003: POI text is not present in the Daashboard screen");
        Assert.assertTrue(TestBase.isElementPresent("Favourites"), "C004: Favourites text is not present in the Daashboard screen");
        Assert.assertTrue(TestBase.isElementPresent("Coordinates"), "C005: Coordinates text is not present in the Daashboard screen");
        Assert.assertTrue(TestBase.isElementPresent("My_Home"), "C006: My_Home text is not present in the Daashboard screen");
        Assert.assertTrue(TestBase.isElementPresent("My_Office"), "C007: My_Office text is not present in the Daashboard screen");
        System.out.println("jj");
        Thread.sleep(2000);
        TestBase.getObject("Back_button").click();
        Thread.sleep(2000);
    }


    // Verifiying Home button pop message in dashboard screen

    @Test(priority = 3)
    public void C003_Check_The_SetHome_Popup() throws Exception {
        //Verify the Home popup text
        Assert.assertTrue(TestBase.isElementPresent("Home_Text"), "C001: Home text is not present in the Set Home popup");
        TestBase.getObject("Home_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\Check_The_SetHome_Popup/" + "Home_Text_Popup " + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("SetNow_Home_Text"), "C001: SetNow_Text is not present in the Set Home popup");
        Assert.assertTrue(TestBase.isElementPresent("No_Button"), "C001: No Button is not present in the Set Home popup");
        Assert.assertTrue(TestBase.isElementPresent("Yes_Button"), "C001: Yes Button is not present in the Set Home popup");
        TestBase.getObject("No_Button").click();
    }


//For verifiy the Office button poup message in dashboard screen

    @Test(priority = 4)
    public void C004_Check_The_SetOffice_Popup() throws Exception {
        Assert.assertTrue(TestBase.isElementPresent("Office_Text"), "C001: Office text is not present in the Set Home popup");
        TestBase.getObject("Office_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C004_Check_The_SetOffice_Popup/" + "Office_Text_Popup " + ".jpg");
        Thread.sleep(2000);
        Assert.assertTrue(TestBase.isElementPresent("SetNow_Office_Text"), "C001: SetNow_Text is not present in the Set Home popup");
        Thread.sleep(2000);
        Assert.assertTrue(TestBase.isElementPresent("No_Button"), "C001: No Button is not present in the Set Home popup");
        Thread.sleep(2000);
        Assert.assertTrue(TestBase.isElementPresent("Yes_Button"), "C001: Yes Button is not present in the Set Home popup");
        Thread.sleep(2000);
        TestBase.getObject("No_Button").click();
    }


// For Favourite screen checking

    @Test(priority = 5)
    public void C005_Check_The_Favourite_Sections() throws Exception {
        Assert.assertTrue(TestBase.isElementPresent("Favourites_button"), "C001: Favourites_text is not present in the Set Home popup");
        TestBase.getObject("Favourites_button").click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C005_Check_The_Favourite_Sections/" + "Favourites_screen_Popup " + ".jpg");
        Thread.sleep(2000);
        TestBase.getObject("Back_button").click();

    }


//For incident checking in dashboard screen

    @Test(priority = 6)
    public void C006_Verify_The_Incident_Section() throws Exception {
        Assert.assertTrue(TestBase.isElementPresent("Incidents_field"), "C001: Favourites_text is not present in the Set Home popup");
        TestBase.getScreenshotForParticularPart("Incidents_field", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C006_Verify_The_Incident_Section/" + "Incidents" + ".jpg");

    }
//For Suggested for you checking in Dashboard screen

    @Test(priority = 7)
    public void C007_Verify_Suggested_For_You_Section() throws Exception {

        Assert.assertTrue(TestBase.isElementPresent("Suggested_For_You_field"), "C001: Suggested_For_You_box is not present in the dashboard");
        TestBase.getScreenshotForParticularPart("Suggested_For_You_field", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C007_Verify_Suggested_For_You_Section/" + "Card1" + ".jpg");
        TestBase.getObject("Suggested_For_You_Text_Button").click();
        TestBase.getScreenshotForParticularPart("Suggested_For_You_field", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C007_Verify_Suggested_For_You_Section/" + "Card2" + ".jpg");
        TestBase.getObject("Suggested_For_You_Text_Button").click();
        TestBase.getScreenshotForParticularPart("Suggested_For_You_field", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C007_Verify_Suggested_For_You_Section/" + "Card3" + ".jpg");
        TestBase.getObject("Suggested_For_You_Text_Button").click();
        TestBase.getScreenshotForParticularPart("Suggested_For_You_field", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C007_Verify_Suggested_For_You_Section/" + "Card4" + ".jpg");
        TestBase.getObject("Suggested_For_You_Text_Button").click();
        TestBase.getScreenshotForParticularPart("Suggested_For_You_field", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C007_Verify_Suggested_For_You_Section/" + "Card5" + ".jpg");
        TestBase.getObject("Suggested_For_You_Text_Button").click();
        TestBase.getScreenshotForParticularPart("Suggested_For_You_field", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C007_Verify_Suggested_For_You_Section/" + "Card6" + ".jpg");
        TestBase.getObject("Suggested_For_You_Text_Button").click();
        TestBase.getScreenshotForParticularPart("Suggested_For_You_field", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C007_Verify_Suggested_For_You_Section/" + "Card7" + ".jpg");
        TestBase.getObject("Suggested_For_You_Text_Button").click();
        TestBase.getScreenshotForParticularPart("Suggested_For_You_field", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C007_Verify_Suggested_For_You_Section/" + "Card8" + ".jpg");
        TestBase.getObject("Suggested_For_You_Text_Button").click();
        TestBase.getScreenshotForParticularPart("Suggested_For_You_field", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C007_Verify_Suggested_For_You_Section/" + "Card9" + ".jpg");
        TestBase.getObject("Suggested_For_You_Text_Button").click();
        TestBase.getScreenshotForParticularPart("Suggested_For_You_field", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C007_Verify_Suggested_For_You_Section/" + "Card10" + ".jpg");
        TestBase.getObject("Suggested_For_You_Text_Button").click();
        TestBase.getScreenshotForParticularPart("Suggested_For_You_field", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C007_Verify_Suggested_For_You_Section/" + "Card11" + ".jpg");
        TestBase.getObject("Suggested_For_You_Text_Button").click();
    }

    @AfterTest
    public void quit(){

        if((TestBase.driver)!=null){
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        TestBase.driver.quit();
        System.out.println("exit");
    }
    }





