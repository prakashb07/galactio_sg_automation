import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

/**
 * Created by QI 33 on 02-05-2016.
 */
public class Global_Search {
    @Test(priority = 1)

    public void C001_Verify_the_Set_Destination_Page() throws Exception {
            TestBase utill = new TestBase();
            utill.initialize();
        TestBase.getObject("Search_option").click();
        TestBase.driver.navigate().back();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\Global_Search\\C001_Verify_the_Set_Destination_Page/" + "Search_Destination_screen" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Recent_button"), "C001:Recent text is not present in the Search_Destination_screen");
        Assert.assertTrue(TestBase.isElementPresent("POI"), "C002: POI text is not present in the Daashboard screen");
        Assert.assertTrue(TestBase.isElementPresent("Favourites"), "C003: Favourites text is not present in the Daashboard screen");
        Assert.assertTrue(TestBase.isElementPresent("Coordinates"), "C004: Coordinates text is not present in the Daashboard screen");
        Assert.assertTrue(TestBase.isElementPresent("Back_button"), "C008: Back_button is not present in the Daashboard screen");
        Thread.sleep(2000);
        Assert.assertTrue(TestBase.isElementPresent("My_Home"), "C005: My_Home text is not present in the Daashboard screen");
        Assert.assertTrue(TestBase.isElementPresent("My_Office"), "C006: My_Office text is not present in the Daashboard screen");
        TestBase.getObject("Back_button").click();
        System.out.println("jj");

    }

    @Test(priority = 2)

    public void C002_Verify_The_SearchDestination_Header() throws Exception{

        TestBase.getObject("Search_option").click();
        Assert.assertTrue(TestBase.isElementPresent("Search_destination_text_field"), "C001: Search text is not present in the  Search Destination Screen");
        Thread.sleep(2000);
        Assert.assertTrue(TestBase.isElementPresent("Voice_recognize_button"),"C002: Voice_recognize_button is not present in the Search Destination Screen");
        TestBase.getObject("Voice_recognize_button").click();
        TestBase.get_Screenshot("src\\test\\resource\\Global_Search\\C002_Verify_The_SearchDestination_Header/" + "Voice_Recognize pop_up box" + ".jpg");
        TestBase.driver.navigate().back();
        Assert.assertTrue(TestBase.isElementPresent("Back_button"),"C003: Back_button is not present in the Search Destination Screen");
        TestBase.getObject("Back_button").click();
    }

//Check for Search option by input data,postal code and address
    @Test(priority = 3)
    public void C003_Check_Search_Input_Criteria_and_ResultSet() throws Exception{
        TestBase.getObject("Search_option").click();
        TestBase.getObject("Search_destination_field").click();
        TestBase.getObject("Search_destination_field").sendKeys(TestBase.TestData.getProperty("Search_Proper_Sequence_Location_1"));
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\Global_Search\\C003_Check_Search_Input_Criteria_and_ResultSet/" + "Suggested search options" + ".jpg");
        TestBase.getObject("Search_destination_data1").click();
        TestBase.getObject("Search_button").click();
        Thread.sleep(5000);
        TestBase.get_Screenshot("src\\test\\resource\\Global_Search\\C003_Check_Search_Input_Criteria_and_ResultSet/" + "Suggested search list1" + ".jpg");
        TestBase.driver.scrollTo("SIM SIANG CHOON BUILDING");
        TestBase.get_Screenshot("src\\test\\resource\\Global_Search\\C003_Check_Search_Input_Criteria_and_ResultSet/" + "Suggested search list2" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.get_Screenshot("src\\test\\resource\\Global_Search\\C003_Check_Search_Input_Criteria_and_ResultSet/" + "Suggested search options after back from result" + ".jpg");
        TestBase.getObject("Clear_Data_button").click();
        TestBase.getObject("Search_destination_field").click();
        TestBase.getObject("Search_destination_field").sendKeys(TestBase.TestData.getProperty("Search_Proper_Sequence_Location_2"));
        Thread.sleep(2000);
        TestBase.getObject("Search_destination_data2").click();
        TestBase.getObject("Search_button").click();
        Thread.sleep(3000);
        TestBase.get_Screenshot("src\\test\\resource\\Global_Search\\C003_Check_Search_Input_Criteria_and_ResultSet/" + "Search_Result_Screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.get_Screenshot("src\\test\\resource\\Global_Search\\C003_Check_Search_Input_Criteria_and_ResultSet/" + "Search_Result_Screen1" + ".jpg");
        TestBase.getObject("Clear_Data_button").click();
        TestBase.getObject("Search_destination_field").click();
        TestBase.getObject("Search_destination_field").sendKeys(TestBase.TestData.getProperty("Postal_Code_Data1"));
        Thread.sleep(3000);
        TestBase.get_Screenshot("src\\test\\resource\\Global_Search\\C003_Check_Search_Input_Criteria_and_ResultSet/" + "Postal_Code1_options1"+ ".jpg");
        Thread.sleep(4000);
        TestBase.getObject("Search_Postal_Code_data1").click();
        TestBase.get_Screenshot("src\\test\\resource\\Global_Search\\C003_Check_Search_Input_Criteria_and_ResultSet/" + "Postal_Code1_options2" + ".jpg");
        TestBase.getObject("Search_button").click();
        Thread.sleep(3000);
        TestBase.get_Screenshot("src\\test\\resource\\Global_Search\\C003_Check_Search_Input_Criteria_and_ResultSet/" + "Postal_Code_Result_List" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Clear_Data_button").click();
        TestBase.getObject("Search_destination_field").click();
        TestBase.getObject("Search_destination_field").sendKeys(TestBase.TestData.getProperty("Address_Search_Data1"));
        TestBase.getObject("Search_button").click();
        Thread.sleep(5000);
        TestBase.get_Screenshot("src\\test\\resource\\Global_Search\\C003_Check_Search_Input_Criteria_and_ResultSet/" + "Address_Search_Result" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Clear_Data_button").click();

    }

//check search option in out of sequence search
    @Test(priority = 4)
    public void C004_Check_Out_Of_Sequence_Search() throws Exception {
        TestBase.getObject("Search_destination_field").click();
        TestBase.getObject("Search_destination_field").sendKeys(TestBase.TestData.getProperty("Search_OutOf_Sequence_Location_1"));
        Thread.sleep(3000);
        TestBase.get_Screenshot("src\\test\\resource\\Global_Search\\C004_Check_Out_Of_Sequence_Search/" + "Search_Result_Available_Options" + ".jpg");
        TestBase.getObject("Search_destination_data1").click();
        TestBase.get_Screenshot("src\\test\\resource\\Global_Search\\C004_Check_Out_Of_Sequence_Search/" + "Search_Result_Available_Options1" + ".jpg");
        TestBase.getObject("Search_destination_data3").click();
        TestBase.get_Screenshot("src\\test\\resource\\Global_Search\\C004_Check_Out_Of_Sequence_Search/" + "Search_Result_Available_Options2" + ".jpg");
        TestBase.getObject("Search_button").click();
        Thread.sleep(3000);
        TestBase.get_Screenshot("src\\test\\resource\\Global_Search\\C004_Check_Out_Of_Sequence_Search/" + "Search_Result_Screen" + ".jpg");

    }
//check the result set
    @Test(priority = 5)
    public void C005_Search_Result_Verification() throws Exception{

        TestBase.get_Screenshot("src\\test\\resource\\Global_Search\\C005_Search_Result_Verification/" + "Search_Result_Screen1" + ".jpg");
        TestBase.driver.scrollTo("AXS - SIM LIM SQ");
        TestBase.get_Screenshot("src\\test\\resource\\Global_Search\\C005_Search_Result_Verification/" + "Search_Result_Screen2" + ".jpg");

    }

    @AfterTest
    public void quit(){

        if((TestBase.driver)!=null){
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        TestBase.driver.quit();
        System.out.println("exit");
    }

}
