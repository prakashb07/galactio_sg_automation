import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by QI 33 on 05-05-2016.
 */
public class ROUTE_PREVIEW_FUNCTION {
    @Test(priority = 1)

    public void C001_Creating_Route_Preview() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();
        TestBase.getObject("Search_option").click();
        TestBase.getObject("POI").click();
        TestBase.getObject("Restaurant").click();
        TestBase.getObject("RestaurantSubcategories1").click();
        TestBase.getObject("RestaurantSubcategories2").click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C001_Creating_Route_Preview/" + "Map view screen By POI" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Preview_Button"), "C001:Preview text is not present in the Map_View_Screen");
        TestBase.getObject("Preview_Button").click();
        Thread.sleep(3000);
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C001_Creating_Route_Preview/" + "Route_Preview_Screen" + ".jpg");

    }
    @Test(priority = 2)

    public void C002_Checking_the_RoutePreview_Screen_Function() throws Exception {
        Assert.assertTrue(TestBase.isElementPresent("Route_Preview_Text"), "C001:Route_Preview text is not present in the header of Route preview screen");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Destination_Text"), "C002:Destination text is not present in the Map_View_Screen");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Destination_Name"), "C003:Destination text is not present in the Map_View_Screen");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Fastest_Text"), "C004:Fastest text is not present in the Map_View_Screen");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Shortest_Text"), "C005:Shortest text is not present in the Map_View_Screen");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Toll_Avoidance_Text"), "C007:Toll_Avoidance text is not present in the Map_View_Screen");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Toll_Minimised_Text"), "C008:Toll_Minimised text is not present in the Map_View_Screen");
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C002_Checking_the_RoutePreview_Screen_Function/" + "swipe1" + ".jpg");
        TouchAction action = new TouchAction(TestBase.driver);

        WebElement element1 = TestBase.getObject("Verify_Toll_Minimised_Text");
        int startY = element1.getLocation().getY() + (element1.getSize().getHeight()/2);
        int startX = element1.getLocation().getX() + (element1.getSize().getWidth()/2);
        WebElement element2 = TestBase.getObject("Verify_Fastest_Text");
        int endX = element2.getLocation().getX() + (element2.getSize().getWidth()/2);
        int endY = element2.getLocation().getY() + (element2.getSize().getHeight()/2);

        action.press(startX, startY).waitAction(2000).moveTo(endX, endY).release().perform();
        Assert.assertTrue(TestBase.isElementPresent("Verify_Traffic_Aware_Text"), "C009:Traffic_Aware text is not present in the Map_View_Screen");
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C002_Checking_the_RoutePreview_Screen_Function/" + "swipe2" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Go_Text"), "C0010:Go_Text is not present in the Map_View_Screen");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Simulate_Text"), "C0011:Simulate_Tex is not present in the Map_View_Screen");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Zoom_Out_button"), "C0012:Zoom_Out_button is not present in the Map_View_Screen");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Zoom_In_button"), "C0013:Zoom_In_button is not present in the Map_View_Screen");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Traffic_Aware_button"), "C0014:Traffic_Aware_button is not present in the Map_View_Screen");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Current_Location_button"), "C0015:Current_Location_button is not present in the Map_View_Screen");
        for(int i=0;i<5;i++){
            TestBase.getObject("Back_button").click();
        }
    }
    @Test(priority = 3)

    public void C003_Checking_the_functionality_for_Route_Preview_Screen() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();
        TestBase.getObject("Search_option").click();
        TestBase.getObject("Search_destination_field").click();
        TestBase.getObject("Search_destination_field").sendKeys(TestBase.TestData.getProperty("Location_1"));
        TestBase.getObject("Search_button").click();
        TestBase.getObject("Location_Id").click();
        TestBase.getObject("Preview_Button").click();
        TestBase.getObject("Verify_Fastest_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C003_Checking_the_functionality_for_Route_Preview_Screen/" + "Fastest_Route_Preview_View_screen_Location_1" + ".jpg");
        TestBase.getObject("Verify_Shortest_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C003_Checking_the_functionality_for_Route_Preview_Screen/" + "Shortest_Route_Preview_View_screen_Location_1" + ".jpg");
        TestBase.getObject("Verify_Toll_Avoidance_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C003_Checking_the_functionality_for_Route_Preview_Screen/" + "Toll_Avoidance_Route_Preview_View_screen_Location_1" + ".jpg");
        TestBase.swipe_function();
        TestBase.getObject("Verify_Toll_Minimised_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C003_Checking_the_functionality_for_Route_Preview_Screen/" + "Toll_Minimised_Route_Preview_View_screen_Location_1" + ".jpg");
        TestBase.getObject("Verify_Traffic_Aware_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C003_Checking_the_functionality_for_Route_Preview_Screen/" + "Traffic_Aware_Route_Preview_View_screen_Location_1" + ".jpg");
        TestBase.getObject("Verify_Traffic_Aware_button").click();
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C003_Checking_the_functionality_for_Route_Preview_Screen/" + "Traffic_Aware_Route_Location_1" + ".jpg");
        for(int i=0;i<=3;i++){
        TestBase.getObject("Verify_Zoom_Out_button").click();}
        TestBase.getObject("Verify_Traffic_Aware_button").click();
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C003_Checking_the_functionality_for_Route_Preview_Screen/" + "Max_Zoom_Out_Route_Preview_Screen" + ".jpg");

        for(int i=0;i<=7;i++){
            TestBase.getObject("Verify_Zoom_In_button").click();}
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C003_Checking_the_functionality_for_Route_Preview_Screen/" + "Max_Zoom_In_Route_Preview_Screen" + ".jpg");
        for(int i=0;i<=3;i++){
            TestBase.getObject("Back_button").click();
        }
        TestBase.getObject("Search_destination_field").click();
        TestBase.getObject("Search_destination_field").sendKeys(TestBase.TestData.getProperty("Location_2"));
        TestBase.getObject("Search_button").click();
        TestBase.getObject("Location_Id").click();
        TestBase.getObject("Preview_Button").click();
        TestBase.getObject("Verify_Fastest_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C003_Checking_the_functionality_for_Route_Preview_Screen/" + "Fastest_Route_Preview_View_screen_Location_2" + ".jpg");
        TestBase.getObject("Verify_Shortest_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C003_Checking_the_functionality_for_Route_Preview_Screen/" + "Shortest_Route_Preview_View_screen_Location_2" + ".jpg");
        TestBase.getObject("Verify_Toll_Avoidance_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C003_Checking_the_functionality_for_Route_Preview_Screen/" + "Toll_Avoidance_Route_Preview_View_screen_Location_2" + ".jpg");
        TestBase.swipe_function();
        TestBase.getObject("Verify_Toll_Minimised_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C003_Checking_the_functionality_for_Route_Preview_Screen/" + "Toll_Minimised_Route_Preview_View_screen_Location_2" + ".jpg");
        TestBase.getObject("Verify_Traffic_Aware_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C003_Checking_the_functionality_for_Route_Preview_Screen/" + "Traffic_Aware_Route_Preview_View_screen_Location_2" + ".jpg");
        for(int i=0;i<=3;i++){
            TestBase.getObject("Back_button").click();
        }
    }


}
