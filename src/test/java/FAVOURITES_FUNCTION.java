/**
 * Created by QI 33 on 04-05-2016.
 */
import io.appium.java_client.TouchAction;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;


public class FAVOURITES_FUNCTION {

        @Test(priority = 1)

        public void C001_Accessing_Favourite_from_Main_Menu() throws Exception {
            TestBase utill = new TestBase();
            utill.initialize();
            Assert.assertTrue(TestBase.isElementPresent("Favourites_button"), "C001: Favourites_text is not present in the Set Home popup");
            TestBase.getObject("Favourites_button").click();
            Thread.sleep(2000);
            TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C001_Accessing_Favourite_from_Main_Menu/" + "Favourites_screen_Page " + ".jpg");
            Thread.sleep(2000);
            TestBase.getObject("Back_button").click();
            TestBase.getObject("Search_option").click();
            TestBase.getObject("POI").click();
            TestBase.getObject("Emergency").click();
            TestBase.getObject("Hospital").click();
            TestBase.getObject("HospitalSubcategories1").click();
            Thread.sleep(3000);
            TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C001_Accessing_Favourite_from_Main_Menu/" + "Favourite_Item_MapView_Page1" + ".jpg");
            TestBase.getObject("Favourite_Star_Button").click();
            TestBase.getObject("Ok_Button").click();

            TestBase.getObject("Back_button").click();
            TestBase.getObject("HospitalSubcategories2").click();
            Thread.sleep(3000);
            TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C001_Accessing_Favourite_from_Main_Menu/" + "Favourite_Item_MapView_Page2" + ".jpg");
            TestBase.getObject("Favourite_Star_Button").click();
            TestBase.getObject("Ok_Button").click();
            for (int i = 1; i <=5; i++) {
                TestBase.getObject("Back_button").click();
            }
            Thread.sleep(2000);
            TestBase.getObject("Favourites").click();
            Thread.sleep(3000);
            TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C001_Accessing_Favourite_from_Main_Menu/" + "Favourites_screen_Page1 " + ".jpg");
            TestBase.getObject("Back_button").click();
            Thread.sleep(2000);
            TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C001_Accessing_Favourite_from_Main_Menu/" + "Favourites_items_On_Suggested For You Box" + ".jpg");
            Thread.sleep(2000);
            TestBase.clear_data();
            TestBase.getObject("Back_button").click();
        }

        @Test(priority = 2)

        public void C002_Accessing_Favourite_from_Search_page() throws Exception {

            TestBase.getObject("Search_option").click();
            TestBase.getObject("Favourites").click();
            Thread.sleep(2000);
            TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C002_Accessing_Favourite_from_Search_page/" + "Favourites_screen_Page " + ".jpg");
            Thread.sleep(2000);
            TestBase.getObject("Back_button").click();
            TestBase.getObject("POI").click();
            TestBase.getObject("Emergency").click();
            TestBase.getObject("Policestation").click();
            TestBase.getObject("PolicestationSubcategories1").click();
            Thread.sleep(3000);
            TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C002_Accessing_Favourite_from_Search_page/" + "Favourite_Item_MapView_Page1" + ".jpg");
            TestBase.getObject("Favourite_Star_Button").click();
            TestBase.getObject("Ok_Button").click();
            TestBase.getObject("Back_button").click();
            TestBase.getObject("PolicestationSubcategories2").click();
            Thread.sleep(3000);
            TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C002_Accessing_Favourite_from_Search_page/" + "Favourite_Item_MapView_Page2" + ".jpg");
            TestBase.getObject("Favourite_Star_Button").click();
            TestBase.getObject("Ok_Button").click();
            for (int i = 1; i < 5; i++) {
                TestBase.getObject("Back_button").click();
            }
            TestBase.getObject("Coordinates").click();
            TestBase.getObject("Latitude").click();
            TestBase.getObject("Latitude").sendKeys(TestBase.TestData.getProperty("Latitude1"));
            TestBase.getObject("Longitude").click();
            TestBase.getObject("Longitude").sendKeys(TestBase.TestData.getProperty("Longitude1"));
            TestBase.getObject("Ok_Button").click();
            TestBase.getObject("Favourite_Star_Button").click();
            TestBase.getObject("Ok_Button").click();
            Thread.sleep(3000);
            TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C002_Accessing_Favourite_from_Search_page/" + "Coordinates Map view screen" + ".jpg");
            for (int i = 1; i <= 2; i++) {
                TestBase.getObject("Back_button").click();
            }
            TestBase.getObject("Favourites").click();
            Thread.sleep(3000);
            TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C002_Accessing_Favourite_from_Search_page/" + "Favourites_screen_Page from search screen " + ".jpg");
            TestBase.getObject("Back_button").click();
            TestBase.getObject("Back_button").click();
            TestBase.clear_data();
            TestBase.getObject("Back_button").click();
        }

        @Test(priority = 3)

        public void C003_Checking_the_display_of_Favourite_window() throws Exception {

            TestBase.getObject("Favourites_button").click();
            Thread.sleep(2000);
            TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C003_Checking_the_display_of_Favourite_window/" + "Checking_Blank_Data_In_Favourites_screen_Page from dashboard " + ".jpg");
            TestBase.getObject("Back_button").click();
            TestBase.getObject("Search_option").click();
            TestBase.getObject("Favourites").click();
            Thread.sleep(2000);
            TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C003_Checking_the_display_of_Favourite_window/" + "Checking_Blank_Data_In_Favourites_screen_Page from search screen " + ".jpg");
            TestBase.getObject("Back_button").click();
            TestBase.getObject("Search_destination_field").click();
            TestBase.getObject("Search_destination_field").sendKeys(TestBase.TestData.getProperty("Location_1"));
            TestBase.selectFavourite();
            TestBase.getObject("Clear_Data_button").click();
            TestBase.getObject("Search_destination_field").click();
            TestBase.getObject("Search_destination_field").sendKeys(TestBase.TestData.getProperty("Location_2"));
            TestBase.selectFavourite();
            TestBase.getObject("Back_button").click();
            TestBase.getObject("Favourites").click();
            Thread.sleep(2000);
            TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C003_Checking_the_display_of_Favourite_window/" + "Favourite screen with data from search screen " + ".jpg");
            TestBase.getObject("Back_button").click();
            TestBase.getObject("Back_button").click();
            TestBase.getObject("Favourites").click();
            Thread.sleep(2000);
            TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C003_Checking_the_display_of_Favourite_window/" + "Favourite screen with data from dashboard" + ".jpg");
            TestBase.getObject("Back_button").click();
            TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C003_Checking_the_display_of_Favourite_window/" + "Favourite data in suggested for you box" + ".jpg");
            Thread.sleep(2000);
            TestBase.clear_data();
            TestBase.getObject("Back_button").click();
        }

        @Test(priority = 4)

        public void C004_Adding_Location_In_Favourite_List() throws Exception {

            TestBase.getObject("Search_option").click();
            TestBase.getObject("Search_destination_field").click();
            TestBase.getObject("Search_destination_field").sendKeys(TestBase.TestData.getProperty("Location_3"));
            TestBase.selectFavourite();
            TestBase.getObject("Clear_Data_button").click();
            TestBase.getObject("Search_destination_field").click();
            TestBase.getObject("Search_destination_field").sendKeys(TestBase.TestData.getProperty("Location_4"));
            TestBase.selectFavourite();
            for (int i = 1; i <= 2; i++) {
                TestBase.getObject("Back_button").click();
            }
        }

        @Test(priority = 5)

        public void C005_Checking_The_Successfull_Addition_In_Favourite_List() throws Exception {

            TestBase.getObject("Favourites").click();
            Thread.sleep(2000);
            TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C005_Checking_The_Successfull_Addition_In_Favourite_List/" + "Favourite screen with data from dashboard" + ".jpg");
            TestBase.getObject("Back_button").click();
            TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C005_Checking_The_Successfull_Addition_In_Favourite_List/" + "Favourite data in Suggested for you box" + ".jpg");
            TestBase.getObject("Search_option").click();
            TestBase.getObject("Favourites").click();
            Thread.sleep(2000);
            TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C005_Checking_The_Successfull_Addition_In_Favourite_List/" + "Favourite screen with data from dashboard" + ".jpg");
            TestBase.getObject("Back_button").click();
            TestBase.getObject("Back_button").click();
            Thread.sleep(2000);
            TestBase.clear_data();
            TestBase.getObject("Back_button").click();
        }

        @Test(priority = 6)

        public void C006_Check_The_Sorting_Functionality() throws Exception {


            TestBase.getObject("Search_option").click();
            TestBase.getObject("Search_destination_field").click();
            TestBase.getObject("Search_destination_field").sendKeys(TestBase.TestData.getProperty("Location_5"));
            TestBase.selectFavourite();
            TestBase.getObject("Clear_Data_button").click();
            TestBase.getObject("Search_destination_field").click();
            TestBase.getObject("Search_destination_field").sendKeys(TestBase.TestData.getProperty("Location_6"));
            TestBase.selectFavourite();
            TestBase.getObject("Clear_Data_button").click();
            TestBase.getObject("Search_destination_field").click();
            TestBase.getObject("Search_destination_field").sendKeys(TestBase.TestData.getProperty("Location_7"));
            TestBase.selectFavourite();
            TestBase.getObject("Clear_Data_button").click();
            TestBase.getObject("Search_destination_field").click();
            TestBase.getObject("Search_destination_field").sendKeys(TestBase.TestData.getProperty("Location_8"));
            TestBase.selectFavourite();
            TestBase.getObject("Back_button").click();
            TestBase.getObject("Favourites").click();
            Thread.sleep(3000);
            TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C006_Check_The_Sorting_Functionality/" + "Favourite data from search screen" + ".jpg");
            TestBase.getObject("Sorting_button").click();
            TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C006_Check_The_Sorting_Functionality/" + "Favourite Sort By pop_up" + ".jpg");
            Assert.assertTrue(TestBase.isElementPresent("Sort_By_Time"), "C002:Time text is not present in the Search_Destination_screen");
            TestBase.getObject("Sort_By_Time").click();
            TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C006_Check_The_Sorting_Functionality/" + "Favourite Data Sort By Time" + ".jpg");
            TestBase.getObject("Sorting_button").click();
            Assert.assertTrue(TestBase.isElementPresent("Sort_By_Distance"), "C002:Distance text is not present in the Search_Destination_screen");
            TestBase.getObject("Sort_By_Distance").click();
            Thread.sleep(2000);
            TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C006_Check_The_Sorting_Functionality/" + "Favourite Data Sort By Distance" + ".jpg");
            TestBase.getObject("Sorting_button").click();
            Assert.assertTrue(TestBase.isElementPresent("Sort_By_Frequency"), "C002:Frequency text is not present in the Search_Destination_screen");
            TestBase.getObject("Sort_By_Frequency").click();
            Thread.sleep(2000);
            TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C006_Check_The_Sorting_Functionality/" + "Favourite Data Sort By Frequency" + ".jpg");
            TestBase.getObject("Back_button").click();
        }

        @Test(priority = 7)

        public void C007_Check_The_Edit_functionality_In_Favourite() throws Exception {

            TestBase.getObject("Favourites").click();
            Thread.sleep(2000);
            TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C007_Check_The_Edit_functionality_In_Favourite/" + "Favourite data from search screen" + ".jpg");
            TouchAction action = new TouchAction(TestBase.driver);
            action.longPress(TestBase.getObject("Edit_Location_1")).perform();
            TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C007_Check_The_Edit_functionality_In_Favourite/" + "Favourite Edit pop up" + ".jpg");
            Assert.assertTrue(TestBase.isElementPresent("Verify_Edit_Favourite_Text"), "C002:Delete_Favourite text is not present in the Search_Destination_screen");
            TestBase.getObject("Edit_Favourites_Button").click();
            TestBase.getObject("Edit_Field").clear();
            TestBase.getObject("Edit_Field").sendKeys(TestBase.TestData.getProperty("Edit_Favourite_name1"));
            TestBase.getObject("Ok_Button").click();
            action.longPress(TestBase.getObject("Edit_Location_2")).perform();
            TestBase.getObject("Edit_Favourites_Button").click();
            TestBase.getObject("Edit_Field").clear();
            TestBase.getObject("Edit_Field").sendKeys(TestBase.TestData.getProperty("Edit_Favourite_name2"));
            TestBase.getObject("Ok_Button").click();
            TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C007_Check_The_Edit_functionality_In_Favourite/" + "Favourite Name change after editing" + ".jpg");
            TestBase.getObject("Back_button").click();
        }

        @Test(priority = 8)

        public void C008_Check_The_Delete_functionality_In_Favourite() throws Exception {

            TestBase.getObject("Favourites").click();
            Thread.sleep(2000);
            TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C008_Check_The_Delete_functionality_In_Favourite/" + "Favourite data from search screen" + ".jpg");
            TouchAction action = new TouchAction(TestBase.driver);
            action.longPress(TestBase.getObject("Delete_Location_1")).perform();
            TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C008_Check_The_Delete_functionality_In_Favourite/" + "Favourite Delete pop up" + ".jpg");
            TestBase.getObject("Delete_Favourite_Button").click();
            Assert.assertTrue(TestBase.isElementPresent("Verify_Delete_Favourite_Text"), "C002:Delete_Favourite text is not present in the Search_Destination_screen");
            Assert.assertTrue(TestBase.isElementPresent("Are_You_Delete_This_Text"), "C002:Are_You_Delete_This text is not present in the Search_Destination_screen");
            TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C008_Check_The_Delete_functionality_In_Favourite/" + "Delete pop up" + ".jpg");
            TestBase.getObject("Ok_Button").click();
            action.longPress(TestBase.getObject("Delete_Location_2")).perform();
            TestBase.getObject("Delete_Favourite_Button").click();
            TestBase.getObject("Ok_Button").click();
            TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C008_Check_The_Delete_functionality_In_Favourite/" + "Favourite list after deleting" + ".jpg");
            TestBase.getObject("Back_button").click();
        }
        @Test(priority = 9)

        public void C009_Checking_The_Back_Function() throws Exception {
            TestBase.getObject("Search_option").click();
            TestBase.getObject("Favourites").click();
            TestBase.getObject("Back_button").click();
            TestBase.getObject("Back_button").click();
            TestBase.clear_data();
            TestBase.getObject("Back_button").click();
        }


    @AfterTest
    public void quit(){

        if((TestBase.driver)!=null){
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        TestBase.driver.quit();
        System.out.println("exit");
    }
}

