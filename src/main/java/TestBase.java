
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import org.apache.commons.io.FileUtils;

import org.openqa.selenium.*;
import org.openqa.selenium.remote.DesiredCapabilities;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;



public class TestBase {
    public static AndroidDriver<MobileElement> driver = null;
    public static Properties config, OR, TestData;
    public static String out = null;
    Dimension size;


    public void initialize() throws Exception {

        // load the config file

        FileInputStream fs1 = new FileInputStream(System.getProperty("user.dir") + "\\src\\main\\resources\\config.properties");
        config = new Properties();
        config.load(fs1);

        ///load my xpaths

        FileInputStream fs2 = new FileInputStream(
                System.getProperty("user.dir") + "\\src\\main\\resources\\objectRepository.properties");
        OR = new Properties();
        OR.load(fs2);

        // load the test data sheet

        FileInputStream fs3 = new FileInputStream(
                System.getProperty("user.dir") + "\\src\\main\\resources\\TestData.properties");

        TestData = new Properties();
        TestData.load(fs3);

        File app = new File("E:\\GalactioSG\\MobileSg-r-a272f78.apk");
        System.out.println("hhhhhhh");
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("deviceName", config.getProperty("28A4AA2C2IE08I2"));
        capabilities.setCapability("platformVersion", config.getProperty("android_version"));
        capabilities.setCapability("platformName", config.getProperty("platform_name"));
        //capabilities.setCapability("appPackage", "com.fortysevendeg.android.swipelistview");
        //capabilities.setCapability("appActivity","com.fortysevendeg.android.swipelistview.sample.activities.SwipeListViewExampleActivity");
        capabilities.setCapability("app", app.getAbsolutePath());
        driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        Thread.sleep(3000);
        TestBase.get_Screenshot("src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\check_splash_screen/" + "splash_screen" + ".jpg");
        System.out.println("j");
    }

    // For Verifiying the xpath
    public static WebElement getObject(String xpathKey) {
        try {
            return driver.findElementByAndroidUIAutomator(OR.getProperty(xpathKey));
        } catch (Throwable t) {
            return null;
        }

    }

    //For screenshot
    public static void get_Screenshot(String path) throws Exception {
        out = new SimpleDateFormat("yyyy-MM-dd_hh_mm_ss").format(new Date());
        File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        File fileOutPut = new File(path);
        FileUtils.copyFile(src, fileOutPut);
    }

    // for verifing the text is present or not
    public static boolean isElementPresent(String xpath) {
        try {
            driver.findElementByAndroidUIAutomator(OR.getProperty(xpath));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static File getScreenshot() {
        out = new SimpleDateFormat("yyyy-MM-dd_hh_mm_ss").format(new Date());
        return driver.getScreenshotAs(OutputType.FILE);
    }

    public static void getScreenshotForParticularPart(String imagePosition, String fileoutputpath) throws IOException {
        WebElement ele = TestBase.getObject(imagePosition);
        File screenshot = TestBase.getScreenshot();
        BufferedImage fullImg = ImageIO.read(screenshot);
        // Get the location of element on the page
        Point point = ele.getLocation();
        // Get width and height of the element
        int eleWidth = ele.getSize().getWidth();
        int eleHeight = ele.getSize().getHeight();
        // Crop the entire page screenshot to get only element screenshot
        BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth, eleHeight);
        ImageIO.write(eleScreenshot, "jpg", screenshot);
        // Copy the element screenshot to disk
        File fileOutPut = new File(fileoutputpath);
        FileUtils.copyFile(screenshot, fileOutPut);


    }

    public static void clear_data() {
        TestBase.getObject("Option_Button").click();
        TestBase.driver.scrollTo("Settings");
        TestBase.getObject("Settings_Button").click();
        TestBase.driver.scrollTo("Clear Data");
        TestBase.getObject("ClearData_Button").click();
        TestBase.getObject("Clear_Recent").click();
        TestBase.getObject("Clear_Favourite").click();
        TestBase.getObject("Clear_Home").click();
        TestBase.getObject("Clear_Office").click();
        TestBase.getObject("Ok_Button").click();

    }

    public static void selectFavourite() {
        TestBase.getObject("Search_button").click();
        TestBase.getObject("Location_Id").click();
        TestBase.getObject("Favourite_Star_Button").click();
        TestBase.getObject("Ok_Button").click();
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

    }

    public static void swipe_function() {
        TouchAction action = new TouchAction(TestBase.driver);
        WebElement element1 = TestBase.getObject("Verify_Toll_Minimised_Text");
        int startY = element1.getLocation().getY() + (element1.getSize().getHeight()/2);
        int startX = element1.getLocation().getX() + (element1.getSize().getWidth()/2);
        WebElement element2 = TestBase.getObject("Verify_Toll_Avoidance_Text");
        int endX = element2.getLocation().getX() + (element2.getSize().getWidth()/2);
        int endY = element2.getLocation().getY() + (element2.getSize().getHeight()/2);
        action.press(startX, startY).waitAction(2000).moveTo(endX, endY).release().perform();
    }
}

